/*
Navicat MySQL Data Transfer

Source Server         : wyq
Source Server Version : 50627
Source Host           : localhost:3306
Source Database       : secondhand

Target Server Type    : MYSQL
Target Server Version : 50627
File Encoding         : 65001

Date: 2020-12-14 22:59:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for order2
-- ----------------------------
DROP TABLE IF EXISTS `order2`;
CREATE TABLE `order2` (
  `o_Id` varchar(100) NOT NULL,
  `o_ItemId` int(255) DEFAULT NULL,
  `o_Seller` varchar(100) DEFAULT NULL,
  `o_Buyer` varchar(100) DEFAULT NULL,
  `o_Baddress` varchar(100) DEFAULT NULL,
  `o_Saddress` varchar(100) DEFAULT NULL,
  `o_Date` date DEFAULT NULL,
  `o_Status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`o_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order2
-- ----------------------------
INSERT INTO `order2` VALUES ('o01', '90001', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '0');
INSERT INTO `order2` VALUES ('o02', '90002', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '1');
INSERT INTO `order2` VALUES ('o03', '90003', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '0');
INSERT INTO `order2` VALUES ('o04', '90004', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '3');
INSERT INTO `order2` VALUES ('o05', '90005', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '1');
INSERT INTO `order2` VALUES ('o06', '90005', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '0');
INSERT INTO `order2` VALUES ('o07', '90007', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '3');
INSERT INTO `order2` VALUES ('o08', '90008', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '0');
INSERT INTO `order2` VALUES ('o09', '90009', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '1');
INSERT INTO `order2` VALUES ('o10', '90010', '1001', '1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学北校区', null, '0');
