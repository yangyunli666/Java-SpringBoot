/*
Navicat MySQL Data Transfer

Source Server         : wyq
Source Server Version : 50627
Source Host           : localhost:3306
Source Database       : secondhand

Target Server Type    : MYSQL
Target Server Version : 50627
File Encoding         : 65001

Date: 2020-12-14 22:59:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `a_Account` varchar(100) NOT NULL,
  `a_Address1` varchar(100) NOT NULL,
  `a_Address2` varchar(100) DEFAULT NULL,
  `a_Address3` varchar(100) DEFAULT NULL,
  `a_Address4` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of address
-- ----------------------------
INSERT INTO `address` VALUES ('1003', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1004', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学北校区');
INSERT INTO `address` VALUES ('1005', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1006', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1007', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1008', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1009', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1010', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1001', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学东校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区');
INSERT INTO `address` VALUES ('1002', '贵州省贵阳市贵州大学北校区', '贵州省贵阳市贵州大学南校区', '贵州省贵阳市贵州大学西校区', '贵州省贵阳市贵州大学东校区');
INSERT INTO `address` VALUES ('1015', '无', '无', '无', '无');
INSERT INTO `address` VALUES ('admin', '无', '无', '无', '无');
