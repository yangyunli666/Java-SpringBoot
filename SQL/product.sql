/*
Navicat MySQL Data Transfer

Source Server         : wyq
Source Server Version : 50627
Source Host           : localhost:3306
Source Database       : secondhand

Target Server Type    : MYSQL
Target Server Version : 50627
File Encoding         : 65001

Date: 2020-12-14 22:59:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `p_Id` int(100) NOT NULL AUTO_INCREMENT,
  `p_Account` varchar(100) DEFAULT NULL,
  `p_Name` varchar(100) DEFAULT NULL,
  `c_Id` varchar(100) DEFAULT NULL,
  `p_Title` varchar(100) DEFAULT NULL,
  `p_Des` varchar(100) DEFAULT NULL,
  `p_Price` double DEFAULT NULL,
  `p_Date` date DEFAULT NULL,
  `p_href` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`p_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=90018 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('90001', '1001', '屏幕', 'c26', '9成新飞利浦27英寸显示器', '27英寸,16:9屏幕比例，60hz刷新频率', '2000', '2020-12-01', '/images/product/90001.jpg');
INSERT INTO `product` VALUES ('90002', '1001', '笔记本电脑', 'c01', '9成新暗影精灵4笔记本电脑', '16.5英寸屏幕,16gb运行内存，1TB+258GB硬盘组合，i5', '3500', null, '/images/product/90002.jpg');
INSERT INTO `product` VALUES ('90003', '1001', '雨伞', 'c06', '全新雨伞，买来没用过', '伞比较大，能撑三个人', '10', null, '/images/product/90003.jpg');
INSERT INTO `product` VALUES ('90004', '1001', '手机', 'c22', '8成新华为P40', '256g储存内存，外观背壳有点磨损，不仔细看的话看不出', '3000', null, '/images/product/90004.jpg');
INSERT INTO `product` VALUES ('90005', '1001', '书籍', 'c29', '深入理解计算机系统，全新', '全新，膜都没撕', '20', null, '/images/product/90005.jpg');
INSERT INTO `product` VALUES ('90006', '1001', '音乐专辑', 'c30', '林俊杰《幸存者·如你》实体专辑，全新，便宜出', '基本算是全新，就拆开来看过几次', '100', null, '/images/product/90006.jpg');
INSERT INTO `product` VALUES ('90007', '1001', '音乐专辑', 'c30', '林俊杰《伟大的渺小》黑胶专辑，全新，便宜出', '基本算是全新，就拆开来看过几次。歌词本保存完好', '200', null, '/images/product/90007.jpg');
INSERT INTO `product` VALUES ('90008', '1001', '键盘', 'c24', '8成新cherry键盘', '8成新cherry键盘，红轴', '300', null, '/images/product/90008.jpg');
INSERT INTO `product` VALUES ('90009', '1001', '相机', 'c23', '8成新Sony ILCE-6400L', '8成新，外观有磨损，不影响使用', '5000', null, '/images/product/90009.jpg');
INSERT INTO `product` VALUES ('90010', '1001', '耳机', 'c32', '9成新sony无线耳机', '9成新，外观完美。', '500', null, '/images/product/90010.jpg');
INSERT INTO `product` VALUES ('90011', '1002', '屏幕', 'c26', '9成三星31.5英寸4K显示器', '31.5英寸,16:9屏幕比例，60hz刷新频率', '1500', null, '/images/product/90011.jpg');
INSERT INTO `product` VALUES ('90012', '1002', '笔记本电脑', 'c01', '9成新华硕VIvoBook14', '16.5英寸屏幕,16gb运行内存，1TB+258GB硬盘组合，i5', '3599', null, '/images/product/90012.jpg');
INSERT INTO `product` VALUES ('90013', '1002', '雨伞', 'c06', '天堂伞大号', '伞比较大，能撑三个人', '10', null, '/images/product/90013.jpg');
INSERT INTO `product` VALUES ('90014', '1002', '手机', 'c22', '8成新iphone11无锁', '256g储存内存，外观背壳有点磨损，不仔细看的话看不出', '3000', null, '/images/product/90014.png');
INSERT INTO `product` VALUES ('90015', '1002', '图书', 'c29', '深入理解计算机系统，全新', '全新，膜都没撕', '20', null, '/images/product/90015.jpg');
INSERT INTO `product` VALUES ('90016', '1002', '音乐专辑', 'c30', '林俊杰《幸存者·如你》实体专辑，全新，便宜出', '基本算是全新，就拆开来看过几次', '99', null, '/images/product/90016.png');
INSERT INTO `product` VALUES ('90017', '1002', '音乐专辑', 'c30', '林俊杰《伟大的渺小》黑胶专辑，全新，便宜出', '基本算是全新，就拆开来看过几次。歌词本保存完好', '198', null, '/images/product/90017.jpg');
